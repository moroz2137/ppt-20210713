## Pipeline model in Elixir

### 勉強會 by Karol Moroz
### 愛吠的狗, July 13, 2021


## What are pipelines? (1)

Most of you have already seen code with a lot of function calls chained using the pipe operator (`|>`). Below is a SQL query written using `Ecto.Query`:

```elixir
from(r in Refund)
|> where([r], r.contract_id in ^refund_ids)
|> join(:inner, [r], ld in assoc(r, :liquidated_damages_order))
|> join(:inner, [r, ld], p in assoc(ld, :payments))
|> group_by([r], r.contract_id)
|> select([r, ld, p], {r.contract_id, max(p.fulfilled_at)})
|> Repo.all()
|> Map.new()
```


## What are pipelines? (2)

Another example of a pipeline are changesets, which are used to cast and validate structured data:

```elixir
def changeset(customer, attrs \\ %{}) do
  customer
  |> cast(attrs, @cast)
  |> cast_assoc(:personal_info, with: &PersonalInfo.changeset/2)
  |> cast_assoc(:customer_id_photos, with: &IDPhoto.changeset/2)
  |> maybe_set_customer_no()
  |> validate_required(@required)
end
```


## What are pipelines? (3)

But the biggest 🐘 in the room are plugs, handling HTTP calls.
In a traditional MVC application using Phoenix, you would write code like this:

```elixir
def create(conn, %{"email" => email, "password" => password}) do
  case Users.authenticate_user_by_email_password(email, password) do
    {:ok, user} ->
      conn
      |> put_session(:user_id, user.id)
      |> put_flash(:success, "Welcome back, #{user.name}!")
      |> redirect(to: "/")

    {:error, reason} ->
      conn
      |> put_flash(:error, reason)
      |> render("new.html")
  end
end
```


## Rule of 👍🏿

In a pipeline, every line is a function that takes a value of a specific type, performs some modifications,
and returns a value of the same type.

For instance, a controller takes a `Plug.Conn` and returns a `Plug.Conn`.

And, you bet, functions operating on `Ecto.Query` take an `Ecto.Query` and must return an `Ecto.Query`, just like with `Ecto.Changeset`.


### What is in a `Plug.Conn`

`Plug.Conn` is a struct that contains all data related to an HTTP request
(not: WebSocket, FTP, UDP, etc.).

Things you always get on a `Plug.Conn`: `host` (e.g. `localhost`), `method` (`GET`, `POST`, etc.), `request_path` (`/api`), `port`, `remote_ip`,
`req_headers`, `scheme` (`:http` or `:https`), `query_string` (`q=papierz&other=2137`).

Fetchable: cookies, body, uploaded files.  
Phoenix parses all of them by default, but you can use Plug without Phoenix.


### Interacting with `Plug.Conn`

Using functions from `Plug.Conn`, e.g.: `assign/3`, `put_status/2`, `put_session/3`, `put_resp_cookie/4`, `put_resp_header/3`, `halt/1`, `put_private/3`.

Or from `Phoenix.Controller`: `put_flash/3`, `render/3`, `redirect/2`, `json/2`.


## Function plugs

A function plug is any function that takes a `Plug.Conn` and returns
a `Plug.Conn`.

```elixir
# Phoenix.Controller.put_layout/2
def put_layout(%Plug.Conn{state: state} = conn, layout) do
  if state in @unsent do
    do_put_layout(conn, :phoenix_layout, layout)
  else
    raise AlreadySentError
  end
end
```


### Module plugs

Module plugs need to implement `init/1` which takes the options passed
to the plug in the pipeline. `init/1` is called **at compile time**.

```elixir
# Example
plug MyAppWeb.Plugs.RestrictAccess, required_roles: [:admin, :manager]

# init gets: [{:required_roles, [:admin, :manager]}]
```

`call/2` takes a `Plug.Conn` and the return value of `init/1`.


### Module plugs

```elixir
defmodule BagnoletteWeb.Plugs.RestrictAccess do
  @behaviour Plug

  import Phoenix.Controller, only: [redirect: 2, put_flash: 3]
  alias BagnoletteWeb.Router.Helpers, as: Routes
  alias Bagnolette.Users.User
  import BagnoletteWeb.Gettext

  def init(opts), do: opts

  def call(%Plug.Conn{} = conn, _) do
    case conn.assigns do
      %{current_user: %User{}} ->
        conn

      %{current_user: nil} ->
        conn
        |> put_flash(:info, gettext("Please sign in to continue."))
        |> redirect(to: Routes.sign_in_path(conn, :new))
    end
  end
end
```


### Places to put plugs

```elixir
# In your router
pipeline :browser do
  plug :accepts, ["html"]
  plug :fetch_session
  plug :fetch_flash
  plug :protect_from_forgery
  plug :put_secure_browser_headers
  plug BagnoletteWeb.Plugs.FetchUser
end
```


### Places to put plugs

```elixir
# In a controller
defmodule BagnoletteWeb.SessionController do
  use BagnoletteWeb, :controller

  plug :put_layout, {BagnoletteWeb.LayoutView, :plain}

  # ...
end
```


### In the endpoint module

```elixir
defmodule BagnoletteWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :bagnolette

  # ...

  plug Plug.RequestId
  plug Plug.Telemetry, event_prefix: [:phoenix, :endpoint]

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head
  plug Plug.Session, @session_options

  plug CORSPlug

  plug BagnoletteWeb.Router
end
```


### Hang on, what???

```elixir
plug BagnoletteWeb.Router
```

If you look carefully, you will notice that `BagnoletteWeb.Router` implements `init/1` and `call/2`:

```elixir
# BagnoletteWeb.Router.module_info()
[
  module: BagnoletteWeb.Router,
  exports: [
    # ... api/2, browser/2, restricted/2 are pipelines - function plugs
    api: 2,
    browser: 2,
    restricted: 2,
    module_info: 0,
    module_info: 1,
    call: 2,
    init: 1
  ],
  # ...
]
```


### Router is a module plug

So, the router is a module plug. What about controllers?

```elixir
# BagnoletteWeb.RentalController.module_info(:exports)
[
  __info__: 1,
  action: 2,
  call: 2,
  init: 1,
  new: 2,
  module_info: 0,
  module_info: 1
]
```


### Oh, so controllers are plugs, too.

What about the endpoint?

```elixir
# BagnoletteWeb.Endpoint.module_info(:exports)
[
  # ...
  call: 2,
  init: 1,
  init: 2,
  # ...
]
```


### Woo, this came so quickly

Like a tornado!

<img src="https://img.itw01.com/images/2018/06/05/16/5918_BGAg1Z_F5GBSED.jpg!r800x0.jpg" />


### Pipeline model

* Execution always goes from top to bottom
* Only the returned value matters
* All information in one place.


## What this means for us

This means that the most basic parts of a Web application&mdash;HTTP processing, data validation, and SQL queries&mdash;have no side effects and rely on pure functions.

Since there are no side effects and all results live in simple structs, applications are easy to reason about and test.


### Compared to Rails

* No idea where `offer` comes from
* `rescue` is last in code, first to run
* `redirect_to` does not return
* passing variables to templates

```ruby
def show
  redirect_to offer, status: :moved_permanently if request.path != offer_path(offer)
  offer.increment!(:views) unless offer.company_id == current_user&.id
  @company = offer.company
  @title = t('.title') + offer.title + ', ' + offer.locations.first.only_city_format
rescue ActiveRecord::RecordNotFound
  redirect_to jobs_outdated_offer_path
end
```


### Traditional MERN

* Requires explicit error handling (`catch`)
* No indication of mutable/immutable data types

```javascript
router.post('/users', function(req, res, next){
  var user = new User();

  user.username = req.body.user.username;
  user.email = req.body.user.email;
  user.setPassword(req.body.user.password);

  user.save().then(function(){
    return res.json({user: user.toAuthJSON()});
  }).catch(next);
});
```
<small>Source: https://github.com/gothinkster/node-express-realworld-example-app/blob/master/routes/api/users.js</small>


## Dankon por via atento.

謝謝各位的聆聽。
